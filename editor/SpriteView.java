/*
  ###########################################################################
  #  This file is part of SpritePAKA                                        #                               
  #                                                                         #
  #  SpritePAKA is free software: you can redistribute it and/or modify     # 
  #  it under the terms of the GNU General Public License as published by   #
  #  the Free Software Foundation, either version 3 of the License, or      #
  #  (at your option) any later version.                                    #
  #                                                                         #
  #  This program is distributed in the hope that it will be useful,        #
  #  but WITHOUT ANY WARRANTY; without even the implied warranty of         #
  #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
  #  GNU General Public License for more details.                           #
  #                                                                         #
  #  You should have received a copy of the GNU General Public License      #
  #  along with this program.  If not, see <http://www.gnu.org/licenses/>   #
  ###########################################################################
*/


import javax.imageio.*;
import java.util.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import Sprites.SpriteSheet;

class SpriteView extends Component{

    private BufferedImage current_image = null;
    private SpriteSheet current_spritesheet = null;
    private boolean looping = false;
    private Thread draw_thread = null;
    private byte fps = 0;
    
    //Every frame will be drawn here
    public void paint (Graphics g){
	Graphics2D g2 = (Graphics2D) g;
	Dimension size = getSize();

	if(current_image != null)
	    g2.drawImage(current_image,
			 0, 0, //src x, y
			 size.width, size.height,
			 0, 0, //dst x, y
			 current_image.getWidth(null),
			 current_image.getHeight(null),
			 null);
    }
    
    public void setCurrentImage(BufferedImage img){
	if(looping)
	    looping = false;
	this.current_image = img;
	repaint();
    }

    public void setSpriteSheet(SpriteSheet spr, byte fps){
	looping = true; this.fps = fps;
	spr.setFps(fps);
	this.current_spritesheet = spr;
	if(draw_thread != null)
	    draw_thread.stop();
	draw_thread = new Thread(new DrawThread(fps));
	draw_thread.start();
	       
    }

    public class  DrawThread implements Runnable{
	private long tick_start = 0;
	private long tick_end = 0;
	private long longest_wait = 0;
	private long wait_time = 0;
	public DrawThread(long fps){
	    longest_wait = 1000 / fps;
	}
	
	public void run(){
	    if(current_spritesheet.getSpriteCount() == 0){
		System.out.println("No sprites in sprite sheet!");
			return;
	    }
	    while(looping){
		tick_start = System.currentTimeMillis();
		current_image = current_spritesheet.next();
		repaint();
		 Toolkit.getDefaultToolkit().sync();
		tick_end = System.currentTimeMillis();
		try{
		    wait_time = longest_wait - (tick_end - tick_start);
		    if(wait_time > 0)
			Thread.sleep(wait_time);
		}catch(InterruptedException exp){
		    System.out.println("Draw thread was interupter!");
		}
	    }
	}
    }
    
}
    
