/*
  ###########################################################################
  #  This file is part of SpritePAKA                                        #                               
  #                                                                         #
  #  SpritePAKA is free software: you can redistribute it and/or modify     # 
  #  it under the terms of the GNU General Public License as published by   #
  #  the Free Software Foundation, either version 3 of the License, or      #
  #  (at your option) any later version.                                    #
  #                                                                         #
  #  This program is distributed in the hope that it will be useful,        #
  #  but WITHOUT ANY WARRANTY; without even the implied warranty of         #
  #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
  #  GNU General Public License for more details.                           #
  #                                                                         #
  #  You should have received a copy of the GNU General Public License      #
  #  along with this program.  If not, see <http://www.gnu.org/licenses/>   #
  ###########################################################################
*/


// This class contains Commonly used methods, only static methods fit here.
package Sprites;
    
import java.awt.Color;
import javax.swing.JFrame;

public class Common{
    private static  JFrame firstFrame = null;

    public static void setFirstFrame(JFrame f){
	firstFrame = f;
    }

    public static JFrame getFirstFrame(){
	return firstFrame;
    }
    
    public static Color optimalForegroundColor(Color back){
	double brightness = Math.sqrt( 0.299 * Math.pow(back.getRed(), 2)
				      + 0.587 * Math.pow(back.getGreen(), 2)
				      + 0.114 * Math.pow(back.getBlue(), 2 ));
	int optimal = 255 - (int) Math.floor(brightness * 255);
	if(optimal > 255)
	    optimal = 255;
	else if(optimal < 0)
	    optimal = 0;
	return new Color(255-optimal, 255-optimal, 255-optimal);
    }
    
    public static Color sharpReduceColor(Color a, double factor){
	factor = -factor;
	double red = Math.pow(a.getRed(), 2),
	    green = Math.pow(a.getGreen(), 2),
	    blue = Math.pow(a.getBlue(), 2);
	
	double sfactor = Math.pow((double)factor / 255, 2);
	if(factor > red)
	    red = 0;
	else
	    red = Math.sqrt(red * factor);
	if(factor > green)
	    green = 0;
	else
	    green = Math.sqrt(green * factor);
	if(factor > blue)
	    blue = 0;
	else
	    blue = Math.sqrt(blue * factor);
	return new Color((int)red,(int)green,(int)blue);
    }

    public static Color smoothReduceColor(Color c, double factor){
	double red = c.getRed(), green = c.getGreen(), blue = c.getBlue();
	factor = factor < 0 ? -factor + 1 : factor;
	red = (red / 100) * factor;
	green = (green / 100) * factor;
	blue = (blue / 100) * factor;



	return new Color((int) red, (int)green, (int)blue);
    }
}
