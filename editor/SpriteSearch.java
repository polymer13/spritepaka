/*
  ###########################################################################
  #  This file is part of SpritePAKA                                        #                               
  #                                                                         #
  #  SpritePAKA is free software: you can redistribute it and/or modify     # 
  #  it under the terms of the GNU General Public License as published by   #
  #  the Free Software Foundation, either version 3 of the License, or      #
  #  (at your option) any later version.                                    #
  #                                                                         #
  #  This program is distributed in the hope that it will be useful,        #
  #  but WITHOUT ANY WARRANTY; without even the implied warranty of         #
  #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
  #  GNU General Public License for more details.                           #
  #                                                                         #
  #  You should have received a copy of the GNU General Public License      #
  #  along with this program.  If not, see <http://www.gnu.org/licenses/>   #
  ###########################################################################
*/
package Sprites;

import java.nio.file.*;
import java.io.File;
import java.lang.Exception;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import java.util.*;
import java.util.regex.*;


public class SpriteSearch{

    public class SpriteLeader{
	private File leader_file;
	private ArrayList<File> children;
	public SpriteLeader(File leader_file){
	    this.leader_file = leader_file;
	    children = new ArrayList<File>();
	}

	public String getName(){
	    return this.leader_file.getName();
	}

	public ArrayList<File> getAllFiles(){
	    ArrayList<File> temp = new ArrayList<File>(children.size() + 1);
	    temp.add(leader_file);
	    for(File x: children)
		temp.add(x);
	    return temp;	    
	}

	public File getLeader(){
	    return leader_file;
	}

	public ArrayList<File> getChildren(){
	    return children;
	}

	public void addChild(File n){
	    children.add(n);
	}

	public void reserve(int space){
	    children.ensureCapacity(children.size() + space);
	}

	public int size(){
	    return children.size() + 1;
	}
	
    }

    
    private boolean allocated = false;
    private boolean is_sorted = false;

    private File base_dir;
    private ArrayList<File> jpg_files;
    private ArrayList<File> png_files;
    private ArrayList<SpriteLeader> leaders;

    public SpriteSearch(){
	jpg_files = new ArrayList<File>();
	png_files = new ArrayList<File>();
    }

    public SpriteLeader getLeaderByString(String name) throws SpriteSearchException{
	for(SpriteLeader x: leaders){
	    if(x.getLeader().getName().equals(name))
		return x;
	}

	throw new SpriteSearchException("Could not find " + name + " as a leader!", ExpCode.NO_LEADER_FOUND);
    }


    public static enum ExpCode{
	CANNOT_READ_DIR, NO_AVAILABLE_SPRTITES, NO_READ_ACCESS,
	NO_AVAILABLE_SPRITES, NOT_A_DIRECTORY, NO_ELEMENTS,
	ARRAY_LISTS_NOT_ALLOCATED, NO_LEADER_FOUND
    }
    
    public static class SpriteSearchException extends Exception{

	private String errorMessage;
	private ExpCode exp;
	public SpriteSearchException(String mess, ExpCode exp ){
	    this.errorMessage = mess;
	    this.exp = exp;
	}
	public String getMessage(){
	    return errorMessage;
	}
	public ExpCode  getExpCode(){
	    return exp;
	}
	
    }


    private static int difference_index(CharSequence s1, CharSequence s2){
	if(s1  == null || s2 == null)
	    return 0;
	int i;
	for(i = 0;i < s1.length();i++){
	    if(i >= s2.length())
		return i;
	    if(s1.charAt(i) == s2.charAt(i))
		continue;
	    else
		return i;
	}
	return i;
    }

    
    private static boolean inside(int needle, ArrayList<Integer> haystack){
	for(Integer x: haystack)
	    if(x.equals(needle))
		return true;
	return false;
    }

    private LinkedList<ArrayList<File>> sort_to_groups(){
	ArrayList<File> lst = new ArrayList<File>(png_files);
	LinkedList<ArrayList<File>> msgs = new LinkedList<ArrayList<File>>();
	ArrayList removed = new ArrayList<File>(lst.size());
	int res ;
	int indx;
	for(int x = 0; x < lst.size();x++){
	    if (inside(x, removed))
		continue;
	    msgs.add(new ArrayList());
	    indx = msgs.size()-1;
	    msgs.get(indx).add(lst.get(x));
	    for(int y = x +1; y < lst.size();y++){
		if(inside(x, removed))
		   continue;
		res = difference_index(lst.get(x).getName(),
				       lst.get(y).getName());
		if(res > 0){
		    removed.add(y);
		    msgs.get(indx).add(lst.get(y));
		}
		
	    }
	}
	for(ArrayList x: msgs)
	    Collections.sort(x);

	return msgs;
    }


    private void createSpriteLeaders(LinkedList<ArrayList<File>> groups)
	throws SpriteSearchException{
	if(groups.isEmpty())
	    throw new SpriteSearchException("No sprites found in directory!", ExpCode.NO_ELEMENTS);

	leaders = new ArrayList<SpriteLeader>(groups.size());
	SpriteLeader temp;
	for(ArrayList<File> x: groups){
	    temp = new SpriteLeader(x.get(0));
	    temp.reserve(x.size());
	
	    for(int i = 1; i < x.size();i++)
		temp.addChild(x.get(i));
	    leaders.add(temp);
	}
	
	
    }

    public void sortIntoSprites() throws SpriteSearchException{
	    LinkedList<ArrayList<File>> groups = sort_to_groups();
	    this.createSpriteLeaders(groups);
	    this.is_sorted = true;
    }
    
    public static SpriteSearch search(final File dir)
	throws SpriteSearchException{
	if(!dir.isDirectory()){
	    throw new SpriteSearchException(dir.toString()
					    + " is not readable!",
					    ExpCode.CANNOT_READ_DIR);
	}

	SpriteSearch temp_spr = new SpriteSearch();

	temp_spr.base_dir = dir;
	
	Pattern png_pattern = Pattern.compile("\\.png$");
	Pattern jpg_pattern = Pattern.compile("\\.jpg$");

	Matcher png_match = null, jpg_match = null;
	String temp_str = null;
	for(final File entry: dir.listFiles()){
	    if(entry.isFile()){
		temp_str = entry.toString();
		png_match = png_pattern.matcher(temp_str);
		if(png_match.find()){
		    temp_spr.png_files.add(entry);
		    continue;
		}
   		jpg_match = jpg_pattern.matcher(temp_str);
		if(jpg_match.find()){
		    temp_spr.jpg_files.add(entry);
		    continue;
		}
	    }
	}
	
	temp_spr.allocated = true;
	//	temp_spr.allocated = true;
	return temp_spr;
    }



    public void fillModelListAll(DefaultListModel mod)
	throws SpriteSearchException{
	if(allocated){
	    if(png_files.size() == 0 && jpg_files.size() == 0){
		throw new SpriteSearchException("No PNG or JPG elements present!", ExpCode.ARRAY_LISTS_NOT_ALLOCATED);
	    }
	    int len = png_files.size() + jpg_files.size();
	    mod.ensureCapacity(len);
	    //System.out.println(len);

	    for(File x: png_files){
		mod.addElement(x.getName());
	    }
	    for(File x: jpg_files){
		mod.addElement(x.getName());
	    }
	    
	
	}else{
	    throw new SpriteSearchException("No elements found",
					    ExpCode.NO_ELEMENTS);
	}

    }

    
    public void fillModelListLeaders(DefaultListModel mod)
	throws SpriteSearchException{
	if(!this.is_sorted){
	    this.sortIntoSprites();
	    
	int len = leaders.size();
	mod.ensureCapacity(len);
	//System.out.println(len);

	for(SpriteLeader x: leaders){
	    mod.addElement(x.getName());
	}
	
	}else{
	    throw new SpriteSearchException("No elements found",
					    ExpCode.NO_ELEMENTS);
    }

}
}


