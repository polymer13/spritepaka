/*
  ###########################################################################
  #  This file is part of SpritePAKA                                        #                               
  #                                                                         #
  #  SpritePAKA is free software: you can redistribute it and/or modify     # 
  #  it under the terms of the GNU General Public License as published by   #
  #  the Free Software Foundation, either version 3 of the License, or      #
  #  (at your option) any later version.                                    #
  #                                                                         #
  #  This program is distributed in the hope that it will be useful,        #
  #  but WITHOUT ANY WARRANTY; without even the implied warranty of         #
  #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
  #  GNU General Public License for more details.                           #
  #                                                                         #
  #  You should have received a copy of the GNU General Public License      #
  #  along with this program.  If not, see <http://www.gnu.org/licenses/>   #
  ###########################################################################
*/

package Sprites;



import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.Transformer;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.*;
import java.awt.image.*;
import java.awt.*;
import javax.imageio.*;
import java.io.*;
import java.lang.Exception;
import Sprites.SpriteSearch.SpriteLeader;
import java.awt.Dimension;
import javax.swing.*;

public class SpriteSheet{
    

    public static enum SpriteOp{
	CROP_TO_SMALLEST, TOP, BOTTOM, CENTER, LEFT, RIGHT
    }
    
    public static enum ExpCode{
	INEQUAL_SPRITE_WIDTH, INEQUAL_SPRITE_HEIGHT,INEQUAL_DIMENSIONS,
	FAILED_TO_READ_FILES, RESULT_IMAGE_TOO_LARGE,
    }
    
    public static class SpriteSheetException extends Exception{

	private String errorMessage;
	private ExpCode exp;
	public SpriteSheetException(String mess, ExpCode exp ){
	    this.errorMessage = mess;
	    this.exp = exp;
	}
	public String getMessage(){
	    return errorMessage;
	}
	public ExpCode  getExpCode(){
	    return exp;
	}

	public boolean is(ExpCode e){
	    if(e == exp)
		return true;
	    else
		return false;
	}
	
    }
    
    // Collections and Containers here
    private ArrayList<BufferedImage> sprites;
    private BufferedImage compiled = null;

    // All atomic values here
    private boolean is_compiled = false;
    private boolean is_loaded = false;
    private boolean multi_file = true;
    private Integer row_width;
    private Integer row_length;
    private Integer column_height;
    private Integer column_length;
    private Integer single_width;
    private Integer single_height;
    private Integer current_step = 0;
    private byte framerate = 30;



    public void filterout_color(Color col) {
	int filter = col.getRGB();
	int t_rgb = 0;
	for(BufferedImage img: sprites){
	    for(int x = 0; x < img.getWidth(); x++)
		for(int y = 0; y < img.getHeight(); y++){
		    t_rgb = img.getRGB(x,y);
		    if((filter >> 8) == (t_rgb >> 8)){
			t_rgb |= 0x000000ff;
			img.setRGB(x, y, t_rgb);
		    }
		}
	}
    }
    
    
    public void write_config(File ofile, String sheet_name) throws IOException {
	if( ofile == null )
	    throw new IOException("NULL file has been provided!");
	PrintWriter cfg = new PrintWriter(ofile);
	//Deprecated in favor of XML
	//cfg.format("single_width:%d%nsingle_height:%d%nrow_size:%d%nrow_width:%d%nsprite_count:%d%noptimal_fps:%d",
	//	   single_width,single_height,row_length,row_width,sprites.size(),framerate);
	try{
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document doc = builder.newDocument();

	    Element sheet = doc.createElement("spritesheet");
	    doc.appendChild(sheet);
	
	    Attr spr_width = doc.createAttribute("width");
	    spr_width.setValue(row_width.toString());
	    Attr spr_height = doc.createAttribute("height");
	    spr_width.setValue(column_height.toString());
	
	    Attr sheet_src = doc.createAttribute("src");
	    sheet_src.setValue(sheet_name);
	    sheet.setAttributeNode(sheet_src);

	    Element single = doc.createElement("single");
	    Attr s_width = doc.createAttribute("width");
	    s_width.setValue(single_width.toString());
	    Attr s_height = doc.createAttribute("height");
	    s_height.setValue(single_width.toString());
	    Attr s_count = doc.createAttribute("count");
	    s_count.setValue(((Integer) sprites.size()).toString());

	    single.setAttributeNode(s_width);
	    single.setAttributeNode(s_height);	
	    single.setAttributeNode(s_count);
	
	    Element row = doc.createElement("row");
	    Attr r_width = doc.createAttribute("width");
	    r_width.setValue(row_width.toString());
	    Attr r_length = doc.createAttribute("length");
	    r_width.setValue(row_length.toString());
	    row.appendChild(single);

	    sheet.appendChild(row);

	    TransformerFactory trans_factory = TransformerFactory.newInstance();
	    Transformer trans = trans_factory.newTransformer();
	    trans.setOutputProperty(OutputKeys.INDENT, "yes");
	    DOMSource source = new DOMSource(doc);
	    StreamResult res = new StreamResult(cfg);
	    trans.transform(source, res);
	}catch( ParserConfigurationException pexp ){
	    System.out.println(pexp.getMessage());
	    throw new IOException("Could not parse spritesheet xml!");
	}catch( TransformerConfigurationException tcexp ){
	    System.out.println(tcexp.getMessage());
	    throw new IOException("Could not create transfomer config for xml!");
	}catch( TransformerException texp ){
	    System.out.println(texp.getMessage());
	    throw new IOException("Could xml transfomation failed!");
	}
	cfg.flush();
	cfg.close();
    }

    public BufferedImage next(){
	if(current_step >= sprites.size())
	    current_step = 0;
	if(sprites.size() == 0)
	    System.out.println("SpriteSheet is NULL");
	BufferedImage a = sprites.get(current_step);
	current_step++;
	return a;
    }

    public void setFps(byte fps){
	this.framerate = fps;
    }

    public byte getFps(byte fps){
	return this.framerate;
    }
    
    public boolean isCompiled(){
	return is_compiled;
    }

    private int getSmallestWidth(){
	int w = Integer.MAX_VALUE;
	int temp;
	for(BufferedImage b: sprites)
	    if((temp = b.getWidth()) < w)
		w = temp;
	return w;
    }

    private int getSmallestHeight(){
	int h = Integer.MAX_VALUE;
	int temp;
	for(BufferedImage b: sprites)
	    if((temp = b.getHeight()) < h)
		h = temp;
	return h;
    }
    
    private void getIdealDimension(int in_row) throws SpriteSheetException{
	boolean same_width, same_height;
	int ow = sprites.get(0).getWidth(), oh = sprites.get(0).getHeight();
	int width, height;
	for(BufferedImage b: sprites){
	    if(ow != b.getWidth()){
		same_width = false;
		throw new SpriteSheetException("Sprites have different width!", ExpCode.INEQUAL_SPRITE_WIDTH);
	    }
	    if(oh != b.getHeight()){
		same_height = false;
		throw new SpriteSheetException("Sprites have different width!", ExpCode.INEQUAL_SPRITE_HEIGHT);
	    }
	}
	width = in_row * ow;
	single_width = ow;
	
	row_width = width;
	row_length = in_row;

	single_height = oh;
	column_length = (int)Math.ceil((double)sprites.size() / (double)in_row);
	column_height = column_length * single_height;
	    
	height = sprites.size() / in_row;
	if(sprites.size() < in_row)
	    width = sprites.size();
	if(column_height < single_height)
	    column_height = single_height+1;
    }
    
    /*
      Cropping is done to make packing of sprites easier!
    */
    private void crop_height(SpriteOp op, SpriteOp from){
	if(from != SpriteOp.TOP && from != SpriteOp.BOTTOM)
	    crop_height(op, SpriteOp.TOP);
	
	ArrayList<BufferedImage> sprs = new ArrayList<BufferedImage>(sprites.size());
	if(op == SpriteOp.CROP_TO_SMALLEST){
	    int smallest = getSmallestHeight();
	    int width, height, x ,y;
	    if(from == SpriteOp.TOP ){
		//Cropping from TOP to BOTTOM
		for(int i = 0;i < sprites.size();i++){
		    width = sprites.get(i).getWidth();
		    sprites.set(i,sprites.get(i).getSubimage(0,0,width, smallest));
		}
	    }else if(from == SpriteOp.BOTTOM){
		//Cropping from BOTTOM to TOP
		for(int i = 0;i < sprites.size();i++){
		    width = sprites.get(i).getWidth();
		    y = sprites.get(i).getHeight() - smallest;
		    sprites.set(i,sprites.get(i).getSubimage(0,y,width, smallest));
		}
	    }else if(from == SpriteOp.CENTER){
		//Cropping around CENTER
		for(int i = 0;i < sprites.size();i++){
		    height = sprites.get(i).getWidth();
		    width = sprites.get(i).getWidth();
		    y = sprites.get(i).getHeight() / 2;
		    sprites.set(i,sprites.get(i).getSubimage(0,y,width, smallest));
		}
	    }
	}
	    
    }

    private void crop_width(SpriteOp op, SpriteOp from){
	if(from != SpriteOp.TOP && from != SpriteOp.BOTTOM)
	    crop_height(op, SpriteOp.TOP);
	
	ArrayList<BufferedImage> sprs = new ArrayList<BufferedImage>(sprites.size());
	if(op == SpriteOp.CROP_TO_SMALLEST){
	    int smallest = getSmallestWidth();
	    int width, height, x ,y;
	    if(from == SpriteOp.LEFT ){
		//Cropping from LEFT to RIGHT
		for(int i = 0;i < sprites.size();i++){
		    height = sprites.get(i).getHeight();
		    sprites.set(i,sprites.get(i).getSubimage(0,0,smallest, height));
		}
	    }else if(from == SpriteOp.RIGHT){
		//Cropping from RIGHT to LEFT
		for(int i = 0;i < sprites.size();i++){
		    height = sprites.get(i).getHeight();
		    x = sprites.get(i).getWidth() - smallest;
		    sprites.set(i,sprites.get(i).getSubimage(0,0,smallest, height));
		}
		
	    }else if(from == SpriteOp.CENTER){
		//Cropping around a CENTER
		for(int i = 0;i < sprites.size();i++){
		    height = sprites.get(i).getHeight();
		    width = sprites.get(i).getWidth();
		    x = width / 4;
		    sprites.set(i,sprites.get(i).getSubimage(0,0,smallest, height));
		}
	    }
	
	}
    }

    private void compileAllPixels(){
	Graphics2D g2 = compiled.createGraphics();
	Graphics g = (Graphics) g2;
	int idx = 0;
	BufferedImage sprt = null;
	int incr = 1;
	if(sprites.size() == 1){
	    int cols = Integer.parseInt(JOptionPane.showInputDialog(Common.getFirstFrame(), "Number of sprites in a columns?"));
	    column_height = cols * single_height;
	    incr = 0;
	}

	for(int y = 0; y < column_height; y += single_height)
	    for(int x = 0; x < row_width;x += single_width){
		if(idx < sprites.size())
		   sprt = sprites.get(idx);
		else
		    break;

		g.drawImage(sprt, x, y, null);


		   idx += incr;
	    }

    }
    
    //This is where the magick happens
    //Well we put images in line
    public void compile() throws SpriteSheetException{

	int row = Integer.parseInt(JOptionPane.showInputDialog(Common.getFirstFrame(), "Number of sprites in a row?"));
	try{
	    getIdealDimension(row);
	    /*GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
	    GraphicsDevice device = env.getDefaultScreenDevice();
	    GraphicsConfiguration config = device.getDefaultConfiguration();
	    Rectangle bound = config.getBounds();
	    //if(bound.getWidth() < row_width || bound.getHeight() < column_height)
	    //	throw new SpriteSheetException("Compiled image is too large", ExpCode.COMPILED_IMAGE_TOO_LARGE);
	    BufferedImage buffy = config.createCompatibleImage(row_width,column_height, Transparency.TRANSLUCENT);*/
	    long res = row_width * column_height;
	    if(res >= Integer.MAX_VALUE)
		throw new SpriteSheetException("Resulting image is too large!", ExpCode.RESULT_IMAGE_TOO_LARGE);
	    compiled =  new BufferedImage(row_width, column_height, BufferedImage.TYPE_INT_ARGB);
	    compileAllPixels();
	    is_compiled = true;
	}catch(SpriteSheetException exp){
	    Object crop_opt[] = {"Crop", "Cancel"};
	    Object height_opt[] = {"From TOP", "Around Center", "From Bottom"};
	    Object width_opt[] = {"From LEFT", "Around Center", "From Bottom"};
	    int st_res = 0,nd_res =0;

	    if(exp.is(ExpCode.INEQUAL_SPRITE_WIDTH)){
		/// WIDTH NEEDS CROPPING
		st_res = JOptionPane.showOptionDialog(Common.getFirstFrame(),"Not all sprites have equal width\nWould you like me to to crop them!", "Crop width!",
						     JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null,crop_opt, crop_opt[0]);


		if(st_res == JOptionPane.YES_OPTION){
		    nd_res = JOptionPane.showOptionDialog(Common.getFirstFrame(), "How would you like me to crop your sprites!", "Copping height from?",
						 JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE,  null,width_opt, width_opt[0]);
		    
		    if(nd_res == JOptionPane.YES_OPTION)
			crop_height(SpriteOp.CROP_TO_SMALLEST, SpriteOp.LEFT);
		    else if(nd_res == JOptionPane.NO_OPTION)
			crop_height(SpriteOp.CROP_TO_SMALLEST, SpriteOp.CENTER);
		    else if(nd_res == JOptionPane.CANCEL_OPTION)
			crop_height(SpriteOp.CROP_TO_SMALLEST, SpriteOp.RIGHT);

		    
		}else if (st_res == JOptionPane.NO_OPTION)
		    //Canceled cropping
		    throw new SpriteSheetException("Inequal width", ExpCode.INEQUAL_DIMENSIONS);
		
	    }else if(exp.is(ExpCode.INEQUAL_SPRITE_HEIGHT)){
		/// HEIGHT NEEDS CROPPING
		st_res = JOptionPane.showOptionDialog(Common.getFirstFrame(),"Not all sprites have equal height\nWould you like me to crop them!",
						     "Crop height!",
						   JOptionPane.YES_NO_OPTION,JOptionPane.PLAIN_MESSAGE,  null,crop_opt, crop_opt[0]);
		
		if(st_res == JOptionPane.YES_OPTION){
		    nd_res = JOptionPane.showOptionDialog(Common.getFirstFrame(), "How would you like me to crop your sprites!", "Copping height from?",
							  JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE,  null,height_opt, height_opt[0]) ;
		    
		    if(nd_res == JOptionPane.YES_OPTION)
			crop_height(SpriteOp.CROP_TO_SMALLEST, SpriteOp.TOP);
		    else if(nd_res == JOptionPane.NO_OPTION)
			crop_height(SpriteOp.CROP_TO_SMALLEST, SpriteOp.CENTER);
		    else if(nd_res == JOptionPane.CANCEL_OPTION)
			crop_height(SpriteOp.CROP_TO_SMALLEST, SpriteOp.BOTTOM);

		}else if(st_res == JOptionPane.NO_OPTION)
		    //Canceled cropping
		    throw new SpriteSheetException("Inequal width", ExpCode.INEQUAL_DIMENSIONS);
	
	    }else
		throw exp;
	}
    }

    public int getRowLength(){
	return row_length;
    }
    
    public int getRowWidth(){
	return row_width;
    }

    public int getNumberOfRows(){
	return column_length;
     }

    public int getSpriteCount(){
	return sprites.size();
    }
    
    public BufferedImage getCompiled() throws SpriteSheetException{
	if(is_compiled)
	    return compiled;
	else{
	    this.compile();
	    return compiled;
	}
    }
    //Loads all files including the leader file
    public void loadSprites(SpriteLeader leader) throws SpriteSheetException{
	sprites = new ArrayList<BufferedImage>(leader.size());
	try{
	    sprites.add(ImageIO.read(leader.getLeader()));
	    for(File x: leader.getChildren()){
		sprites.add(ImageIO.read(x));
	    }
	}catch (IOException exp){
	    throw new SpriteSheetException("Could not read files.\nIOException" + exp.toString(),
					   ExpCode.FAILED_TO_READ_FILES);
	}
	is_loaded = true;
    }
}
