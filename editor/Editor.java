/*
  ###########################################################################
  #  This file is part of SpritePAKA                                        #                               
  #                                                                         #
  #  SpritePAKA is free software: you can redistribute it and/or modify     # 
  #  it under the terms of the GNU General Public License as published by   #
  #  the Free Software Foundation, either version 3 of the License, or      #
  #  (at your option) any later version.                                    #
  #                                                                         #
  #  This program is distributed in the hope that it will be useful,        #
  #  but WITHOUT ANY WARRANTY; without even the implied warranty of         #
  #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
  #  GNU General Public License for more details.                           #
  #                                                                         #
  #  You should have received a copy of the GNU General Public License      #
  #  along with this program.  If not, see <http://www.gnu.org/licenses/>   #
  ###########################################################################
*/

import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.*;
import javax.swing.filechooser.*;
import java.awt.*;
import java.awt.event.*;
import javax.imageio.*;
import java.awt.image.BufferedImage;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.imageio.*;
import java.io.*;
import Sprites.SpriteSearch;
import Sprites.SpriteSheet;
import Sprites.SpriteSheet.*;
import Sprites.SpriteSearch.SpriteLeader;
import Sprites.Common;

class Editor{

    public enum SpriteType{
	IMAGE, SHEET, NONE,
    }
	
    /* Convinience because we don't want to load all images
       When ever somebody clicks an entry in the list. */
    
    public class SpriteData{
	private SpriteType data_type = SpriteType.NONE;
	private BufferedImage img = null;
	private SpriteSheet sheet = null;
        public SpriteData(BufferedImage img){
	    data_type = SpriteType.IMAGE;
	    this.img = img;
	}
	public SpriteData(SpriteSheet sheet){
	    data_type = SpriteType.SHEET;
	    this.sheet = sheet;
	}

	public void setSheet(SpriteSheet sheet){
	    data_type = SpriteType.SHEET;
	    this.img = null;
	    this.sheet = sheet;
	    
	}

	public SpriteSheet getSheet(){
	    return this.sheet;
	}

	public BufferedImage getImage(){
	    return this.img;
	}

	public SpriteType type(){
	    return data_type;
	}
	
    }



    private SpriteOperations local_operations;
    
    //GUI COMPONENTS HERE PLEASE
    //predefine
    private JFrame mainFrame = null;
    private JPanel bottomPanel = null;
    private JPanel topPanel = null;
    private JLabel statusLabel = null;
    private JLabel fileLabel = null;
    private JLabel fpsLabel = null;
    private JList  fileList= null;
    private JScrollPane fileScrollPane = null;
    private JButton openButton = null;
    private JButton cancelButton = null;
    private JButton packButton = null;
    private JButton filterButton = null;
    private JButton saveButton = null;
    private JSpinner fpsSpinner = null;
    //custom
    private SpriteView spriteView = null;


    //Collections and Containers
    
    // All loaded files sorted by directory
    private HashMap<String,HashMap<String, SpriteData>> loaded_image_dirs = null;
    // All loaded files in current directory 
    private HashMap<String,SpriteData> loaded_images;
	
    private BufferedImage current_image  = null;
    private SpriteSearch current_search = null;
    private DefaultListModel fileListModel= null;
    private SpriteLeader current_leader;
    private SpriteSheet current_spritesheet;
    private File last_opened_dir = null;
    private File last_save_dir = null;
    //Scalar values
    private boolean listReadyToUse = false;

    
    
    //COLORS HERE_PLEASE
    private Color base_color;
    private Color list_background;
    private Color panel_background_color;
    private Color viewer_background_color;
    private Color font_color;
    private Color list_font_color;
    private Color list_highlight_color;
    private Color list_highlight_font_color;
    private Color open_button_color;
    private Color filter_button_color;
    private Color cancel_button_color;
    private Color button_font_color;
    private Color save_button_color;
    private Color pack_button_color;
    private Color text_field_color;

    //Initialize basic components
    public Editor(Color base, boolean dark){
	if(Common.getFirstFrame() == null)
	    Common.setFirstFrame(mainFrame);
	prepare_interface();
	prepare_panels();
	prepare_color(base, dark);
	assign_color();
	this.local_operations = new SpriteOperations();
    }

    
    public void prepare_color(Color base, boolean dark){
	double offset = dark ? -3.0 : 1.0;
	if(base.equals(Color.BLACK))
	   base = new Color(5, 5, 5);
	///Init color////////////////////////
	
	this.base_color = base;
	this.panel_background_color = this.base_color;
	this.viewer_background_color = Common.smoothReduceColor(base, offset*70);
	this.list_background = Common.smoothReduceColor(base, offset*30);
	this.text_field_color = Common.smoothReduceColor(base, offset*10);
	this.font_color = Common.optimalForegroundColor(base);
	this.list_font_color = Common.optimalForegroundColor(list_background);
	this.list_highlight_color = Common.smoothReduceColor(base, offset*100);
	this.list_highlight_font_color = Common.optimalForegroundColor(list_highlight_color);
	this.list_highlight_font_color = list_font_color;
	
	//Constant colors////////////////////
	
	this.button_font_color = Color.WHITE;
	String os = System.getProperty("os.name");
	if (os.startsWith("Windows"))
	    button_font_color = base_color;
	this.open_button_color = Color.BLUE;
	this.filter_button_color = new Color(200, 0, 200);
	this.cancel_button_color = Color.RED;
	this.save_button_color = Color.CYAN;
	this.pack_button_color = new Color(0, 200, 0);
	
	/////////////////////////////////////
    }
    
    //Set all default colors here

    /* List elements are created dynamically so colors
       are assigned else where  */

    public void assign_color(){
	mainFrame.getContentPane().setBackground(panel_background_color);

	fileLabel.setForeground(font_color);
	fileList.setBackground(list_background);
	fileList.setForeground(font_color);

	fpsLabel.setForeground(font_color);
	Component fps_text = fpsSpinner.getComponent(0);
	fps_text.setBackground(text_field_color);
	fps_text.setForeground(font_color);

	fps_text = fpsSpinner.getComponent(1);
	fps_text.setBackground(text_field_color);
	fps_text.setForeground(font_color);
	
	fps_text = fpsSpinner.getComponent(2);
	fps_text.setBackground(text_field_color);
	fps_text.setForeground(font_color);


	openButton.setForeground(button_font_color);
	openButton.setBackground(open_button_color);

	saveButton.setForeground(button_font_color);
	saveButton.setBackground(save_button_color);
	
	cancelButton.setForeground(button_font_color);
	cancelButton.setBackground(cancel_button_color);

	filterButton.setForeground(button_font_color);
	filterButton.setBackground(filter_button_color);

	packButton.setForeground(button_font_color);
	packButton.setBackground(pack_button_color);

	topPanel.setBackground(viewer_background_color);
	bottomPanel.setBackground(panel_background_color);

    }

 
    private void prepare_filelist(){
	fileListModel = new DefaultListModel();
	loaded_image_dirs = new HashMap<String, HashMap<String, SpriteData>>();
	fileList = new JList(fileListModel);
	fileList.addListSelectionListener(new FileListListener());
	fileList.setSize(200, 10);
	fileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	fileList.setLayoutOrientation(JList.VERTICAL);
	fileList.setVisibleRowCount(-1);

	fileList.setCellRenderer(new FileListRenderer());
	
	fileScrollPane = new JScrollPane(fileList);
	fileScrollPane.setPreferredSize(new Dimension(300, 200));
    }
       
    private void prepare_interface(){
	mainFrame = new JFrame("SpritePAKA v1.3 Alpha");
	mainFrame.setSize(640, 480);
	mainFrame.setLayout(new BorderLayout());


	//SPRITE VIEWER////////////////////////////////
	spriteView = new SpriteView();

	fileLabel = new JLabel("None", JLabel.LEFT);
	fpsLabel = new JLabel("FPS: ", JLabel.RIGHT);

	prepare_filelist();

	mainFrame.addWindowListener(new WindowAdapter(){
		public void closingWindow(WindowEvent winEv){
		    System.exit(0);
		}
	    });

	GridLayout topPanelLayout =  new GridLayout(2, 1);
	//FlowLayout bottomPanelLayout = new FlowLayout(FlowLayout.LEADING);


	//CONFIGURING PANELS//////////////////
	topPanel = new JPanel();
	topPanelLayout.setVgap(10);

	topPanel.setLayout(topPanelLayout);
	
	
	bottomPanel = new JPanel();
	bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.LINE_AXIS));

	//ADDING TO PANLES////////////////////
	topPanel.add(spriteView);
	mainFrame.add(topPanel, BorderLayout.CENTER);
	mainFrame.add(bottomPanel, BorderLayout.SOUTH);
	mainFrame.add(fileScrollPane, BorderLayout.WEST);
    }

    public void prepare_panels(){
	this.openButton = new JButton("Open directory");
	this.cancelButton = new JButton("Cancel");
	this.packButton = new JButton("Compile");
	this.filterButton = new JButton("Filter color");
	this.saveButton = new JButton("Save");
	this.fpsSpinner = new JSpinner( new SpinnerNumberModel(60, 1, 255, 5) );


	// Assigning actions to buttons
	openButton.setActionCommand("open");
	cancelButton.setActionCommand("cancel");
	packButton.setActionCommand("pack");
	saveButton.setActionCommand("save");
	filterButton.setActionCommand("filter");
	

	//Assigning button event listeners///////////////////
	ButtonClickedListener but_lis = new ButtonClickedListener();
	openButton.addActionListener(but_lis);
	cancelButton.addActionListener(but_lis);
	packButton.addActionListener(but_lis);
	saveButton.addActionListener(but_lis);
	filterButton.addActionListener(but_lis);
	
	openButton.addMouseListener(new MouseEventListener());
	
	bottomPanel.add(openButton);
	bottomPanel.add(cancelButton);
	bottomPanel.add(packButton);
	bottomPanel.add(filterButton);
	bottomPanel.add(fileLabel);

	bottomPanel.add(Box.createHorizontalGlue());
	bottomPanel.add(fpsLabel);
	bottomPanel.add(fpsSpinner);
	bottomPanel.add(saveButton);
	//bottomPanel.add(Box.createRigidArea(new Dimension(10,0)));
	mainFrame.setVisible(true);
    }
    

    public static void main(String[] args){
	//Nice color mixes for the editor
	/*//////////////////////////////
	 * Dark *
	   (50, 10, 30) (30, 10, 50)
	   (60, 5, 5)
	 * Clear *
	   (100, 10, 50)
	   (0, 30, 30)
	*///////////////////////////////

	try {
            // Set cross-platform Java L&F (also called "Metal")
	    UIManager.setLookAndFeel(
				     UIManager.getSystemLookAndFeelClassName());
	} 
	catch (UnsupportedLookAndFeelException e) {
	    // handle exception
	}
	catch (ClassNotFoundException e) {
	    // handle exception
	}
	catch (InstantiationException e) {
	    // handle exception
	}
	catch (IllegalAccessException e) {
	    // handle exception
	}
	
	Editor myeditor = new Editor(new Color(10, 10, 100), false);
    }

    
    public void selectImageByString(String dir,String name){
	try{
	    current_leader = current_search.getLeaderByString(name);
	}catch(SpriteSearch.SpriteSearchException exp){
	    JOptionPane.showMessageDialog(mainFrame,exp.getMessage());
	}
	   
	if(!loaded_images.containsKey(name)){
	    File temp = current_leader.getLeader();
	    try{
		current_image = ImageIO.read(temp);
	    }catch(IOException ioexp){
		JOptionPane.showMessageDialog(mainFrame,"Image I/O exeption while loading" + temp.getName());
	    }
	    loaded_images.put(name, new SpriteData(current_image));
	    spriteView.setCurrentImage(current_image);
	}else{
	    SpriteData temp = loaded_images.get(name);
	    if(temp.type() == SpriteType.IMAGE){
		current_image = temp.getImage();
	    	spriteView.setCurrentImage(current_image);
	    }else if(temp.type() == SpriteType.SHEET){
		current_spritesheet = temp.getSheet();
		spriteView.setSpriteSheet(current_spritesheet,(byte) 60);
	    }else{
		System.out.println("Sprite has no data type!");
	    }
	    
	}

    }

    private class SpriteOperations {
	public void openOperation(){
	    	//OPEN FILE CODE
		listReadyToUse = false;
		fileList.clearSelection();

		JFileChooser chooser = last_opened_dir == null ? new JFileChooser() : new JFileChooser(last_opened_dir);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int result = chooser.showOpenDialog(mainFrame);
		if(result == JFileChooser.APPROVE_OPTION){
		    String dir = chooser.getSelectedFile().getName();
		    fileLabel.setText("Directory: " + dir);

		    // Add directory entry for buffered storage
		    if(loaded_image_dirs.containsKey(dir))
			loaded_images = loaded_image_dirs.get(dir);
		    else{
			loaded_images = new HashMap<String, SpriteData>();
			loaded_image_dirs.put(dir, loaded_images);
		    }

		    
		    last_opened_dir = chooser.getSelectedFile();
		    try{
			current_search = SpriteSearch.search(chooser.getSelectedFile());
			fileListModel.clear();
			
			current_search.fillModelListLeaders(fileListModel);
		    }catch(SpriteSearch.SpriteSearchException exp){
			JOptionPane.showMessageDialog(mainFrame,exp.getMessage());
		    }
		}
		listReadyToUse = true;
	}

	public void packOperation(){
	    	if(current_leader == null)
		    return;
		// Let's get to business
		try{
		    String name = fileList.getSelectedValue().toString();

		    // If images are not loaded load them 
		    if(!loaded_images.containsKey(name)){

			current_spritesheet = new SpriteSheet();
			current_spritesheet.loadSprites(current_leader);
			current_spritesheet.compile();
			current_image = current_spritesheet.getCompiled();

		    }else{
			SpriteData temp = loaded_images.get(name);

			// Else if only partially loaded, load all!
			if(temp.type() == SpriteType.IMAGE){
			    current_spritesheet = new SpriteSheet();
			    current_spritesheet.loadSprites(current_leader);
			    temp.setSheet(current_spritesheet);
			}
		    }

		    //Setting fps for the SpriteViewer and also the .cfg file
		    int fps = (int) fpsSpinner.getValue();
		    if( fps > 255 )
			fps = 255;
		    spriteView.setSpriteSheet(current_spritesheet,(byte) fps);

		    // Writing image for Testing this will be moved
		    //File out = new File("test.png");
		    //ImageIO.write(current_spritesheet.getCompiled(), "png", out);

		}catch(SpriteSheetException spr_exp){
		    System.out.println(spr_exp.getMessage());
		    return;
		}
	}
	
	public void saveOperation(){
	    //OPEN FILE CODE
	    listReadyToUse = false;
	    fileList.clearSelection();

	    JFileChooser chooser = last_opened_dir == null ? new JFileChooser() : new JFileChooser(last_opened_dir);
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

	    int result = chooser.showOpenDialog(mainFrame);
	    if(result == JFileChooser.APPROVE_OPTION){
		String dir = chooser.getSelectedFile().getName();
		fileLabel.setText("Directory: " + dir);

		// Add directory entry for buffered storage
		if(loaded_image_dirs.containsKey(dir))
		    loaded_images = loaded_image_dirs.get(dir);
		else{
		    loaded_images = new HashMap<String, SpriteData>();
		    loaded_image_dirs.put(dir, loaded_images);
		}

		    
		last_opened_dir = chooser.getSelectedFile();
		try{
		    current_search = SpriteSearch.search(chooser.getSelectedFile());
		    fileListModel.clear();
			
		    current_search.fillModelListLeaders(fileListModel);
		}catch(SpriteSearch.SpriteSearchException exp){
		    JOptionPane.showMessageDialog(mainFrame,exp.getMessage());
		} 
	    }
	}

	public void cancelOperation(){
	    System.exit(0);
	}

	public void filterOperation(){
	    if(current_spritesheet != null){
		Color col = JColorChooser.showDialog(mainFrame, "Filter out color!", null);
		if(col != null)
		    current_spritesheet.filterout_color(col);
	    }
	}
	    
    }
    
    ///EVENT LISTENING /////////////////////////////
    
    private class ButtonClickedListener implements ActionListener{

	HashMap<Integer, Thread>  save_threads;
	int index = 0;
	
	public ButtonClickedListener(){
	    save_threads = new HashMap<Integer, Thread>();
	}
	
	public void actionPerformed(ActionEvent evn){
	    String command = evn.getActionCommand();

	    switch(command){
	    case "open":
		local_operations.openOperation();
		break;
	    case "pack":
		local_operations.packOperation();
		break;
	    case "save":
		local_operations.saveOperation();
		break;
	    case "filter":
		local_operations.filterOperation();
		break;
	    case "cancel":
		local_operations.cancelOperation();
		break;
	    default:
		break;
	    }
	}

	public class SavingThread implements Runnable {
	    public File image_file = null;
	    public File config_file = null;
	    public SpriteSheet sheet = null;
	    public int index = 0;
	    
	    public void run(){
		try{
		    ImageIO.write(sheet.getCompiled(), "png", this.image_file);
		    sheet.write_config(this.config_file, image_file.getName());
		    save_threads.remove(this.index);
		}catch(IOException ioexp){
		    JOptionPane.showMessageDialog(mainFrame,"Image I/O error while writing: " + ioexp.getMessage(),
						  null,JOptionPane.ERROR_MESSAGE);
		}catch(SpriteSheetException spr_exp){
		    JOptionPane.showMessageDialog(mainFrame,"Could not compile spritesheet: " + spr_exp.getMessage(),
						  null, JOptionPane.ERROR_MESSAGE);
		}
	    }
	}

    }

    public class FileListRenderer extends JLabel implements ListCellRenderer<Object>{
	public FileListRenderer(){
	    setOpaque(true);
	}

	public Component getListCellRendererComponent(JList<?> list, Object value,
						  int index, boolean isSelected,
						  boolean isFocused)
	{
	    setText(value.toString());
	    Color back = Color.BLACK;
	    Color front = Color.WHITE;
	    JList.DropLocation drop = list.getDropLocation();
	    if(drop != null && !drop.isInsert() && drop.getIndex() == index){
		
	    }else if(isSelected){
		back = list_highlight_color;
		front = list_highlight_font_color;
	    }else{
		back = list_background;
		front = list_font_color;
	    }

	    setBackground(back);
	    setForeground(front);
	    return this;
	}
    }

    private class MouseEventListener implements MouseListener{
	public void mouseEntered(MouseEvent e){

	}
	public void mouseExited(MouseEvent e){

        }
	public void mouseReleased(MouseEvent e){
	    
	}

	public void mousePressed(MouseEvent e){
	    
	}

	public void mouseClicked(MouseEvent e){

	}
    }


    public class FileListListener implements ListSelectionListener{
	public void valueChanged(ListSelectionEvent evt){
	    if(!evt.getValueIsAdjusting() && listReadyToUse){
		//		System.out.println("Selection: " + fileList.getSelectedValue().toString());
		
		selectImageByString(last_opened_dir.getName() ,fileList.getSelectedValue().toString());
	    }
	    /*ListSelectionModel select = (ListSelectionModel) evt.getSource();*/
	}
    }
}
