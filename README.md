## What is this ? ##
SpritePAKA is a small Java program that packs directory containing images into a spritesheet.
It is able to pack multiple sprite groups based on their name, there is no need to specify each file
for the spritesheet.

## Compiling ##
To compile on any platform you will need python 2.X and Scons build system, just navigate to the root
directory and type scons.

